import pathlib
from os import path

import torch
import torchvision.transforms as transforms
from PIL import Image
from torch.utils.data import Dataset


class MNISTDataset(Dataset):
    def __init__(self, X: int, y: int, data_dir: path.abspath) -> None:
        self.X = X
        self.y = y

        self.transform = transforms.Compose(
            [
                # transforms.Resize(self.X, self.Y),
                transforms.ToTensor(),
                # transforms.Normalize((0.5, 0.5, 0.5), (0.5, 0.5, 0.5)),
            ]
        )

        self.image_paths = [str(p) for p in pathlib.Path(data_dir).glob("**/*.jpg")]

    def __len__(self) -> int:
        return len(self.image_paths)

    def __getitem__(self, index: int) -> tuple[torch.Tensor, int]:
        p = self.image_paths[index]
        path = pathlib.Path(p)
        image = Image.open(path)
        image = image.convert("RGB")

        if self.transform:
            out_data = self.transform()

        out_label = int(str(path.parent.name))

        return out_data, out_label
