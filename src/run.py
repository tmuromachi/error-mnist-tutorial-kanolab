import yaml

from train import TrainComponents, evaluate, train
from utils import show_results


def main():
    print("***** Load Parameters *****")
    params_dict = yaml.safe_load(open("./src/args/params.yaml"))

    train_components = TrainComponents()
    train_components.set_train_components(params=params_dict)

    train_loss_list = []
    test_loss_list = []

    print("***** Train *****")
    for epoch in range(params_dict["epoch"]):
        train_loss, test_loss = train(train_components)
        train_loss_list.append(train_loss)
        test_loss_list.append(test_loss)

        print("Epoch: {}".format(epoch))
        print("Train Loss: {}".format(train_loss))
        print("Test Loss: {}".format(test_loss))

    print("***** Check Loss *****")
    # 結果を可視化

    print("***** Evaluate *****")
    scores = evaluate(train_components)

    print("***** Results *****")
    print("Accuracy: {}".format(scores["accuracy"]))
    print("Precision: {}".format(scores["precision"]))
    print("Recall: {}".format(scores["recall"]))
    print("F1: {}".format(scores["f1"]))

    print("***** Save *****")
    train_components.save_components(params_dict["output_dir"])


if __name__ == "__main__":
    main()
