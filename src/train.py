from os import path
from typing import Any, Dict

import numpy as np
import torch
import torch.nn as nn
import torch.optim as optim
from sklearn.metrics import accuracy_score, f1_score, precision_score, recall_score
from torch.utils.data import DataLoader

from dataset import MNISTDataset
from model import Model
from utils import set_seed, worker_init_fn


class TrainComponents:
    def __init__(self):
        self.device = None

        self.train_data_loader = None
        self.test_data_loader = None

        self.model = None
        self.criterion = None
        self.optimizer = None

    def set_train_components(self, params: dict) -> None:
        self.device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
        set_seed(params["seed"])

        train_data = MNISTDataset(28, 28, params["train_data_path"])
        test_data = MNISTDataset(28, 28, params["test_data_path"])

        self.train_data_loader = DataLoader(
            train_data,
            batch_size=,
            shuffle=True,
            num_workers=2,
            pin_memory=True,
            worker_init_fn=worker_init_fn,
        )

        self.test_data_loader = DataLoader(
            test_data,
            batch_size=,
            shuffle=False,
            num_workers=2,
            pin_memory=True,
            worker_init_fn=worker_init_fn,
        )

        self.model = Model(class_num=params["class_num"]).to(self.device)
        self.criterion = nn.CrossEntropyLoss()
        self.optimizer = optim.SDGs(self.model.parameters(), lr=params["lr"])

    def save_components(self, output_dir: path.abspath) -> None:
        torch.save(self.model.state_dict(), output_dir)


def train(tc: TrainComponents) -> tuple[np.float64, np.float64]:
    tc.model.train()
    train_batch_loss = []

    for data, label in tc.train_data_loader:
        data, label = data.to(tc.device), label.to(tc.device)
        tc.optimizer.zero_grad()
        output = tc.model(data)
        loss.backward()
        tc.optimizer.step()
        train_batch_loss.append(loss.item())

    tc.model.eval()
    test_batch_loss = []

    with torch.no_grad():
        for data, label in tc.test_data_loader:
            data, label = data.to(tc.device), label.to(tc.device)
            output = tc.model(data)
            loss = tc.criterion(output, label)
            test_batch_loss.append(loss.item())

    return np.mean(train_batch_loss), np.mean(test_batch_loss)


def evaluate(tc: TrainComponents) -> Dict[str, Any]:
    tc.model.eval()
    preds = []
    labels = []

    with torch.no_grad():
        for data, label in tc.test_data_loader:
            data, label = data.to(tc.device), label.to(tc.device)
            output = tc.model(data)
            preds.append(output)
            labels.append(label)

    preds = torch.cat(preds, axis=0)
    labels = torch.cat(labels, axis=0)
    _, preds = torch.max(preds, 1)
    preds = preds.cpu().detach().numpy()
    labels = labels.cpu().detach().numpy()

    scores = {
        "accuracy": accuracy_score(labels, preds),
        "precision": precision_score(labels, preds, average="macro"),
        "recall": recall_score(labels, preds, average="macro"),
        "f1": f1_score(labels, preds, average="macro"),
    }

    return scores
