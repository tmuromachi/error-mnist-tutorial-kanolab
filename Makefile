run:
	poetry run python src/run.py

tests:
	poetry run isort ./src
	poetry run black ./src
	poetry run pflake8 ./src
